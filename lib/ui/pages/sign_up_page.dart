part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  User user;
  File pictureFile;

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: Dictionary.signUpTitlePage,
      subtitle: Dictionary.signUpSubtitlePage,
      onBackButtonPressed: () {
        Get.back();
      },
      child: Form(
        autovalidateMode: AutovalidateMode.always,
        key: formKey,
        child: Column(
          children: [
            GestureDetector(
              onTap: () async {
                PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
                if (pickedFile != null) {
                  pictureFile = File(pickedFile.path);
                  setState(() {});
                }
              },
              child: Container(
                width: 110,
                height: 110,
                margin: const EdgeInsets.only(top: 26),
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(image: DecorationImage(image: AssetImage('assets/photo_border.png'))),
                child: (pictureFile != null)
                    ? Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, image: DecorationImage(image: FileImage(pictureFile), fit: BoxFit.cover)),
                      )
                    : Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(image: AssetImage('assets/photo.png'), fit: BoxFit.cover)),
                      ),
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              child: Text(
                Dictionary.labelFullName,
                style: blackFontStyle2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 12),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: nameController,
                decoration: InputDecoration(
                  isDense: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                  hintStyle: greyFontStyle,
                  hintText: Dictionary.hintTypeFullName,
                ),
                validator: MultiValidator([
                  RequiredValidator(errorText: Dictionary.requiredForm),
                  MinLengthValidator(3, errorText: Dictionary.minFullName),
                ]),
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              child: Text(
                Dictionary.labelEmail,
                style: blackFontStyle2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 12),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: emailController,
                decoration: InputDecoration(
                  isDense: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                  hintStyle: greyFontStyle,
                  hintText: Dictionary.hintTypeEmail,
                ),
                validator: MultiValidator([
                  RequiredValidator(errorText: Dictionary.requiredForm),
                  EmailValidator(errorText: Dictionary.invalidEmail),
                ]),
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              child: Text(
                Dictionary.labelPassword,
                style: blackFontStyle2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 12),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                obscureText: true,
                controller: passwordController,
                decoration: InputDecoration(
                  isDense: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                  hintStyle: greyFontStyle,
                  hintText: Dictionary.hintTypePassword,
                ),
                validator: MultiValidator([
                  RequiredValidator(errorText: Dictionary.requiredForm),
                  MinLengthValidator(6, errorText: Dictionary.minPassword),
                  MaxLengthValidator(15, errorText: Dictionary.maxPassword),
                ]),
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(top: 24, bottom: 24),
              height: 45,
              padding: const EdgeInsets.symmetric(horizontal: defaultMargin),
              child: RaisedButton(
                onPressed: () {
                  if (formKey.currentState.validate()) {
                    Get.to(AddressPage(
                        User(
                          name: nameController.text,
                          email: emailController.text,
                        ),
                        passwordController.text,
                        pictureFile));
                  }
                },
                elevation: 0,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                color: mainColor,
                child: Text(
                  Dictionary.actionContinue,
                  style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
