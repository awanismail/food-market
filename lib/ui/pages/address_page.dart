part of 'pages.dart';

class AddressPage extends StatefulWidget {
  final User user;
  final String password;
  final File pictureFile;

  AddressPage(this.user, this.password, this.pictureFile);

  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController houseNumController = TextEditingController();
  bool isLoading = false;
  List<String> cities;
  String selectedCity;

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();

    cities = ['Bandung', 'Jakarta', 'Surabaya'];
    selectedCity = cities[0];
  }

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: Dictionary.addressTitlePage,
      subtitle: Dictionary.addressSubtitlePage,
      onBackButtonPressed: () {
        Get.back();
      },
      child: Form(
        autovalidateMode: AutovalidateMode.always,
        key: formKey,
        child: Column(
          children: [
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
              child: Text(
                Dictionary.labelPhoneNumber,
                style: blackFontStyle2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 12),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: phoneController,
                decoration: InputDecoration(
                  isDense: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                  hintStyle: greyFontStyle,
                  hintText: Dictionary.hintTypePhoneNumber,
                ),
                validator: MultiValidator([
                  RequiredValidator(errorText: Dictionary.requiredForm),
                  PatternValidator(r'(^(?:[+0]9)?[0-9]{10,12}$)', errorText: Dictionary.invalidPhoneNumber),
                ]),
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              child: Text(
                Dictionary.labelAddress,
                style: blackFontStyle2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 12),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: addressController,
                decoration: InputDecoration(
                  isDense: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                  hintStyle: greyFontStyle,
                  hintText: Dictionary.hintTypeAddress,
                ),
                validator: RequiredValidator(errorText: Dictionary.requiredForm),
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              child: Text(
                Dictionary.labelHouseNumber,
                style: blackFontStyle2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 12),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: TextFormField(
                controller: houseNumController,
                decoration: InputDecoration(
                  isDense: true,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
                  hintStyle: greyFontStyle,
                  hintText: Dictionary.hintTypeHouseNumber,
                ),
                validator: RequiredValidator(errorText: Dictionary.requiredForm),
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              child: Text(
                Dictionary.labelCity,
                style: blackFontStyle2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(8), border: Border.all(color: Colors.grey)),
              child: DropdownButton(
                  value: selectedCity,
                  isExpanded: true,
                  underline: SizedBox(),
                  items: cities
                      .map((e) => DropdownMenuItem(
                          value: e,
                          child: Text(
                            e,
                            style: blackFontStyle3,
                          )))
                      .toList(),
                  onChanged: (item) {
                    setState(() {
                      selectedCity = item;
                    });
                  }),
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(top: 24, bottom: 24),
              height: 45,
              padding: const EdgeInsets.symmetric(horizontal: defaultMargin),
              child: (isLoading == true)
                  ? Center(
                      child: loadingIndicator,
                    )
                  : RaisedButton(
                      onPressed: () async {
                        if (formKey.currentState.validate()) {
                          User user = widget.user.copyWith(
                              phoneNumber: phoneController.text,
                              address: addressController.text,
                              houseNumber: houseNumController.text,
                              city: selectedCity);

                          setState(() {
                            isLoading = true;
                          });

                          await context.bloc<UserCubit>().signUp(user, widget.password, pictureFile: widget.pictureFile);

                          UserState state = context.bloc<UserCubit>().state;

                          if (state is UserLoaded) {
                            context.bloc<FoodCubit>().getFoods();
                            context.bloc<TransactionCubit>().getTransactions();
                            Get.to(MainPage());
                          } else {
                            Get.snackbar("", "",
                                backgroundColor: "D9435E".toColor(),
                                icon: Icon(
                                  MdiIcons.closeCircleOutline,
                                  color: Colors.white,
                                ),
                                titleText: Text(
                                  Dictionary.errorSignUp,
                                  style: GoogleFonts.poppins(color: Colors.white, fontWeight: FontWeight.w600),
                                ),
                                messageText: Text(
                                  (state as UserLoadingFailed).message,
                                  style: GoogleFonts.poppins(color: Colors.white),
                                ));
                            setState(() {
                              isLoading = false;
                            });
                          }
                        }
                      },
                      elevation: 0,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                      color: mainColor,
                      child: Text(
                        Dictionary.actionSignUpNow,
                        style: GoogleFonts.poppins(color: Colors.black, fontWeight: FontWeight.w500),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
