part of 'pages.dart';

class OrderHistoryPage extends StatefulWidget {
  @override
  _OrderHistoryPageState createState() => _OrderHistoryPageState();
}

class _OrderHistoryPageState extends State<OrderHistoryPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TransactionCubit, TransactionState>(builder: (_, state) {
      if (state is TransactionLoaded) {
        if (state.transactions.length == 0) {
          return IllustrationPage(
            title: Dictionary.orderHistoryEmptyTitlePage,
            subtitle: Dictionary.orderHistoryEmptySubtitlePage,
            picturePath: 'assets/love_burger.png',
            buttonTap1: () {},
            buttonTitle1: Dictionary.actionFindFoods,
          );
        } else {
          double listItemWidth = MediaQuery.of(context).size.width - 2 * defaultMargin;

          return RefreshIndicator(
            onRefresh: () async {
              await context.bloc<TransactionCubit>().getTransactions();
            },
            child: ListView(
              children: [
                Column(
                  children: [
                    //// Header
                    Container(
                      height: 100,
                      width: double.infinity,
                      margin: const EdgeInsets.only(bottom: defaultMargin),
                      padding: const EdgeInsets.symmetric(horizontal: defaultMargin),
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            Dictionary.orderTitlePage,
                            style: blackFontStyle1,
                          ),
                          Text(
                            Dictionary.orderSubtitlePage,
                            style: greyFontStyle.copyWith(fontWeight: FontWeight.w300),
                          )
                        ],
                      ),
                    ),
                    //// Body
                    Container(
                      width: double.infinity,
                      color: Colors.white,
                      child: Column(
                        children: [
                          CustomTabBar(
                            titles: [Dictionary.orderTab1, Dictionary.orderTab2],
                            selectedIndex: selectedIndex,
                            onTap: (index) {
                              setState(() {
                                selectedIndex = index;
                              });
                            },
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Builder(builder: (_) {
                            List<Transaction> transactions = (selectedIndex == 0)
                                ? state.transactions
                                    .where((element) =>
                                        element.status == TransactionStatus.on_delivery ||
                                        element.status == TransactionStatus.pending)
                                    .toList()
                                : state.transactions
                                    .where((element) =>
                                        element.status == TransactionStatus.delivered ||
                                        element.status == TransactionStatus.cancelled)
                                    .toList();

                            return Column(
                              children: transactions
                                  .map((e) => Padding(
                                        padding: const EdgeInsets.only(right: defaultMargin, left: defaultMargin, bottom: 16),
                                        child: GestureDetector(
                                          onTap: () async {
                                            if (e.status == TransactionStatus.pending) {
                                              await launch(e.paymentUrl);
                                            }
                                          },
                                          child: OrderListItem(transaction: e, itemWidth: listItemWidth),
                                        ),
                                      ))
                                  .toList(),
                            );
                          }),
                          SizedBox(
                            height: 60,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          );
        }
      } else {
        return Center(
          child: loadingIndicator,
        );
      }
    });
  }
}
