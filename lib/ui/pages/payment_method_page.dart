part of 'pages.dart';

class PaymentMethodPage extends StatelessWidget {
  final String paymentURL;

  PaymentMethodPage(this.paymentURL);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: IllustrationPage(
          title: Dictionary.paymentMethodTitlePage,
          subtitle: Dictionary.paymentMethodSubtitlePage,
          picturePath: 'assets/Payment.png',
          buttonTap1: () async {
            await launch(paymentURL);
          },
          buttonTitle1: Dictionary.paymentMethodAction1,
          buttonTap2: () {
            Get.to(SuccessOrderPage());
          },
          buttonTitle2: Dictionary.paymentMethodAction2,
        ));
  }
}
