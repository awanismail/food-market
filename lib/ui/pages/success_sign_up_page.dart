part of 'pages.dart';

class SuccessSignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: IllustrationPage(
          title: Dictionary.successSignUpTitlePage,
          subtitle: Dictionary.successSignUpSubtitlePage,
          picturePath: 'assets/food_wishes.png',
          buttonTap1: () {},
          buttonTitle1: Dictionary.successSignUpAction1,
        ));
  }
}
