class Dictionary {
  static const String requiredForm = "* Required";

  // Sign In  & Up Page
  static const String signUpTitlePage = "Sign Up";
  static const String signUpSubtitlePage = "Register and eat";
  static const String signInTitlePage = "Sign In";
  static const String signInSubtitlePage = "Find your best ever meal";
  static const String labelFullName = "Full Name";
  static const String hintTypeFullName = "Type your full name";
  static const String minFullName = "Full name should be atleast 3 characters";
  static const String labelEmail = "Email Address";
  static const String hintTypeEmail = "Type your email address";
  static const String invalidEmail = "Enter valid email id";
  static const String labelPassword = "Password";
  static const String hintTypePassword = "Type your password";
  static const String minPassword = "Password should be atleast 6 characters";
  static const String maxPassword = "Password should not be greater than 15 characters";
  static const String actionSignin = "Sign In";
  static const String actionContinue = "Continue";
  static const String actionNewAccount = "Create New Account";
  static const String errorSignIn = "Sign In Failed";
  static const String errorSignUp = "Sign Up Failed";

  // Success Sign Up Page
  static const String successSignUpTitlePage = "Yeay! Completed";
  static const String successSignUpSubtitlePage = "Now you are able to order\nsome foods as a self reward";
  static const String successSignUpAction1 = "Find Foods";

  // Success Order Page
  static const String successOrderTitlePage = "You've Made Order";
  static const String successOrderSubtitlePage = "Just stay at home while we are\npreparing your best foods";
  static const String successOrderAction1 = "Order Other Foods";
  static const String successOrderAction2 = "View My Order";

  // Profile Page
  static const String profileTab1 = "Account";
  static const String profileTab2 = "FoodMarket";
  static const String profileMenu1 = "Edit Profile";
  static const String profileMenu2 = "Home Address";
  static const String profileMenu3 = "Security";
  static const String profileMenu4 = "Payment";
  static const String profileMenu5 = "Rate App";
  static const String profileMenu6 = "Help Center";
  static const String profileMenu7 = "Privacy & Policy";
  static const String profileMenu8 = "Term & Condition";

  // Food Page
  static const String foodTitlePage = "Food Market";
  static const String foodSubtitlePage = "Let's get some foods";
  static const String foodTab1 = "New Taste";
  static const String foodTab2 = "Popular";
  static const String foodTab3 = "Recommended";

  // Food Detail Page
  static const String foodIngredients = "Ingredients:";
  static const String foodTotalPrice = "Total Price";
  static const String actionOrderNow = "Order Now";

  // Address Page
  static const String addressTitlePage = "Address";
  static const String addressSubtitlePage = "Make sure it's valid";
  static const String labelPhoneNumber = "Adress";
  static const String hintTypePhoneNumber = "Type your phone number";
  static const String invalidPhoneNumber = "Enter a valid phone number";
  static const String labelAddress = "Address";
  static const String hintTypeAddress = "Type your address";
  static const String labelHouseNumber = "House Number";
  static const String hintTypeHouseNumber = "Type your house number";
  static const String labelCity = "City";
  static const String actionSignUpNow = "Sign Up Now";

  // Order History Page
  static const String orderHistoryEmptyTitlePage = "Ouch! Hungry";
  static const String orderHistoryEmptySubtitlePage = "Seems you like have not\nordered any food yet";
  static const String actionFindFoods = "Find Foods";
  static const String orderTitlePage = "Your Orders";
  static const String orderSubtitlePage = "Wait for the best meal";
  static const String orderTab1 = "In Progress";
  static const String orderTab2 = "Past Orders";

  // Payment Page
  static const String paymentTitlePage = "Payment";
  static const String paymentSubtitlePage = "You deserve better meal";
  static const String paymentItemOrdered = "Item Ordered";
  static const String paymentDetailTransaction = "Detail Transactions";
  static const String paymentDriver = "Driver";
  static const String paymentTax = "Tax 10%";
  static const String paymentTotal = "Total";
  static const String paymentDeliver = "Driver to";
  static const String paymentName = "Name";
  static const String paymentPhone = "Phone Number";
  static const String paymentAddress = "Address";
  static const String paymentHouse = "House Number";
  static const String paymentCity = "City";
  static const String paymentFailed = "Transaction Failed";
  static const String paymentTryAgain = "Please try again later.";
  static const String actionCheckoutNow = "Checkout Now";

  // Paymet Method Page
  static const String paymentMethodTitlePage = "Finish Your Payment";
  static const String paymentMethodSubtitlePage = "Please select your favourite\npayment method";
  static const String paymentMethodAction1 = "Payment Method";
  static const String paymentMethodAction2 = "Continue";
}
